

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rescueapp/events-patient/add_patient.dart';
import 'package:rescueapp/events-patient/delete_patient.dart';
import 'package:rescueapp/events-patient/patient_event.dart';
import 'package:rescueapp/events-patient/set_patient.dart';
import 'package:rescueapp/events-patient/update_patient.dart';
import 'package:rescueapp/model-patient/patient.dart';

class PatientBloc extends Bloc<PatientEvent, List<Patient>> {
  @override
  List<Patient> get initialState => List<Patient>();
// events wird definiert durch PatientEvent.
  // mapEventToState wird aufgerufen um das bestimmte event mit einem bestimmten status zu definieren
  @override
  Stream<List<Patient>> mapEventToState(PatientEvent event) async* {
    if (event is SetPatient) {
      yield event.patientList;
    } else if (event is AddPatient) {
      List<Patient> newState = List.from(state);
      if (event.newPatient != null) {
        newState.add(event.newPatient);
      }
      yield newState;
    } else if (event is DeletePatient) {
      List<Patient> newState = List.from(state);
      newState.removeAt(event.patientIndex);
      yield newState;
    } else if (event is UpdatePatient) {
      List<Patient> newState = List.from(state);
      newState[event.patientIndex] = event.newPatient;
      yield newState;
    }
  }
}
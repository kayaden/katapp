import 'package:rescueapp/events-patient/patient_event.dart';
import 'package:rescueapp/model-patient/patient.dart';

class AddPatient extends PatientEvent {
  Patient newPatient;

  AddPatient(Patient patient) {
    newPatient = patient;
  }
}

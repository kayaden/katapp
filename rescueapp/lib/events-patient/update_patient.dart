import 'package:rescueapp/events-patient/patient_event.dart';
import 'package:rescueapp/model-patient/patient.dart';



class UpdatePatient extends PatientEvent {
  Patient newPatient;
  int patientIndex;

  UpdatePatient(int index, Patient patient) {
    newPatient = patient;
    patientIndex = index;
  }
}
import 'package:rescueapp/events-patient/patient_event.dart';
import 'package:rescueapp/model-patient/patient.dart';

class SetPatient extends PatientEvent {
  List<Patient> patientList;

  SetPatient(List<Patient> patient) {
    patientList = patient;
  }
}